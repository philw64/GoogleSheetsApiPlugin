<?php
/*
 * Plugin Name: Google Sheets Api
 * Version: 1.0
 * Plugin URI: http://wiseowlservices.org/
 * Description: Provides a Google Sheets interface
 * Author: Philip Wooldridge
 * Author URI:
 * Requires at least: 5.6
 * Tested up to: 5.6
 *
 * @package WordPress
 * @author Philip Wooldridge
 * @since 1.0.0
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

require_once dirname( __FILE__ )."/logging.php";
require_once dirname( __FILE__ )."/SheetsApi.php";

if ( ! defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'SheetsApiWrapper' ) ) {
	class SheetsApiWrapper {


		/**
		 * This code makes the class a singleton, only instantiated once when
		 * ::Instance() is invoked. Calling Instance again will
		 * return the same instance pointer.
		 */
		public static function Instance()
		{
			static $inst = null;
			if ($inst === null) {
				$inst = new SheetsApiWrapper();
			}
			return $inst;
		}

		public function __construct() {
			$this->sheets_api = GoogleSheetsApi::Instance();
			$logging = Logging::Instance();
			$logging->enable();

			add_filter( 'page_template', array($this, 'test_sheet_page'), 50, 1 );

		}


	    /** Page function to display the /templates/test_sheet_page.php template.
		 * For development only
		 */
	    public function test_sheet_page( $page_template )
	    {
	        if ( is_page( 'test-sheet-page' ) ) {
	                $page_template =  dirname( __DIR__ ) . '/SheetsApiWrapper/templates/test_sheet_page.php';
	        }
	        return $page_template;
	    }

		/**
		 * @param: $spreadsheetId (string) It is part of the sheer URL. For example
		 * URL = https://docs.google.com/spreadsheets/d/---SPREADSHEET ID---/edit#gid=0
		 */
		public function getSheet($spreadsheetId) {
			$this->sheets_api->enableDebug();
			$sheet = $this->sheets_api->doRequest(
				'GET',GoogleSheetsApi::GET_SHEET,
				array(),
				array('spreadsheetId' => $spreadsheetId)
			);
			return $sheet;
		}


		/**
		 * Appends the supplied values below the last row in the identified sheet
		 * @param: $spreadsheetId (string)
		 * @param: $range in A1 format. For example Sheet1!A1
		 * @param: $values: Array. Two dimensional array:
 		 *          [
		 *            ["Row1Cell1","Row1Cell2",...],
		 *            ["Row2Cell1","Row2Cell2",...],
		 *          ]
		 */
		public function appendRows($spreadsheetId,$range,$values) {
			//$this->sheets_api->enableDebug();

			$valueArray = [
			  "range" => $range,
			  "majorDimension"=> "ROWS",
			  "values" => $values,
			];
			$result = $this->sheets_api->doRequest(
				'POST',
				GoogleSheetsApi::APPEND,
				array('valueInputOption'=>'RAW'),//  query parameters
				array('spreadsheetId' => $spreadsheetId, 'range'=>$range),
				$valueArray);

			return $result;
		}

		/**
		 * Updates the value(s) at range
		 * @param: $spreadsheetId (string)
		 * @param: $range in A1 format. For example Sheet1!A1
		 * @param: $values: Array. Two dimensional array:
 		 *          [
		 *            ["Row1Cell1","Row1Cell2",...],
		 *            ["Row2Cell1","Row2Cell2",...],
		 *          ]
		 *
		 */
		public function updateValues($spreadsheetId,$range,$values) {
			//$this->sheets_api->enableDebug();

			$valueArray = [
			  "range" => $range,
			  "majorDimension"=> "ROWS",
			  "values" => $values
			];
			$result = $this->sheets_api->doRequest(
				'PUT',
				GoogleSheetsApi::UPDATE,
				array('valueInputOption'=>'RAW'),//  query parameters
				array('spreadsheetId' => $spreadsheetId, 'range'=>$range),
				$valueArray);

			return $result;
		}

		/**
		 * Loads the value(s) at range in the identified sheet
		 * @param: $spreadsheetId (string)
		 * @param: $range in A1 format. For example Sheet1!A1
		 * @return: $values: Array of array of values:
		 *           [
		 * 		   		["Row1Value1","Row1Value2",...],
		 * 		   		["Row2Value1","Row2Value2",...],
		 *           ]
		 *
		 */
		public function getValues($spreadsheetId,$range) {
			//$this->sheets_api->enableDebug();
			$result = $this->sheets_api->doRequest(
				'GET',
				GoogleSheetsApi::UPDATE,
				array("majorDimension"=> "ROWS"),//  query parameters
				array('spreadsheetId' => $spreadsheetId, 'range'=>$range));

			return $result;
		}
	}
}

$googleSheetsApi = SheetsApiWrapper::Instance();
/* END OF FILE */
?>
