<?php
/*
 * A simple logging function that outputs to the plugins directory
 * Author: Philip Wooldridge
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'Logging' ) ) {
	class Logging {
		/**
		 * This code makes the class a singleton, only instantiated once when
		 * ::Instance() is invoked. Calling Instance again will
		 * return the same instance pointer.
		 */
		public static function Instance()
		{
			static $inst = null;
			if ($inst === null) {
				$inst = new Logging();
			}
			return $inst;
		}

		private function __construct() {
			$this->enabled = false;
		}

		public function enable() {
			$this->enabled = true;
		}

		public function disable() {
			$this->enabled = false;
		}

		function log($prefix, $message) {
			if (!$this->enabled) {return;}

			if(is_array($message)) {
			   $message = json_encode($message);
			}
			$file = fopen(dirname( __FILE__ ) . "/../debug.log","a");
			fwrite($file, "\n($prefix): " . date('Y-m-d G:i:s T') . " :: $message");
			fclose($file);
		}


	}
}
/* END OF FILE */
?>
