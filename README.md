SheetsApiWrapper
================

This is a simple PHP wrapper for calling the Google Sheets APIS. It aims to be
- Simple to use
- LIghtweight (no external dependencies)

What it Does Do
---------------

The class handles OAuth2.0 authentication.

What it Doesn't Do
------------------

Installing 
==========

Download SheetsApiWrapper.php and require this in your code.

How to Get Your API Key
=======================

[See this blog](https://ieg.wnet.org/2015/08/using-oauth-in-wordpress-plugin-part-1-the-basics)

Usage
=====

  
Examples
========

See templates/test_sheet_page.php

TODO
====

