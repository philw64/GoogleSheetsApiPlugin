<?php
/*
 * The interface to the Google Sheets Api
 * Author: Philip Wooldridge
 *
 * Based on a similar API developed for interfacing to Zoom and
 * this Google development resource.
 * https://developers.google.com/sheets/api/reference/rest
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

require_once dirname( __FILE__ )."/GoogleOauth.php";
if ( ! defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'GoogleSheetsApi' ) ) {
	class GoogleSheetsApi {

		const SERVICE_ENDPOINT = 'https://sheets.googleapis.com';
		const GET_SHEET       = '/v4/spreadsheets/{spreadsheetId}';
		const GET_RANGE       = '/v4/spreadsheets/{spreadsheetId}/values/{range}';
		const CREATE          = '/v4/spreadsheets';
		const APPEND          = '/v4/spreadsheets/{spreadsheetId}/values/{range}:append';
		const UPDATE          = '/v4/spreadsheets/{spreadsheetId}/values/{range}';


		private $oauthClient;
		private $timeout;

		/**
		 * This code makes the class a singleton, only instantiated once when
		 * ::Instance() is invoked. Calling Instance again will
		 * return the same instance pointer.
		 */
		public static function Instance()
		{
			static $inst = null;
			if ($inst === null) {
				$inst = new GoogleSheetsApi();
			}
			return $inst;
		}

		public function __construct() {
			$this->oauthClient = OauthClient::Instance();
			$this->sheetsApiDebug = true;
		}

		public function enableDebug() {
			$this->sheetsApiDebug = true;
		}

		public function get_access_token() {
			return $this->oauthClient->get_access_token();
		}

	    private function headers() {
			$access_token = $this->oauthClient->get_access_token();
			return array(
	            'Authorization: Bearer ' . $access_token,
	            'Content-Type: application/json',
	            'Accept: application/json',
	        );
	    }

		/** Replaces params in paths:  /root/path/{params}
		 * @param: $path: string
		 * @param: $requestParams: associative array of 'param'=>value
		 */
	    private function pathReplace( $path, $requestParams ){
	        $errors = array();
	        $path = preg_replace_callback( '/\\{(.*?)\\}/',function( $matches ) use( $requestParams,$errors ) {
	            if (!isset($requestParams[$matches[1]])) {
	                $this->errors[] = 'Required path parameter was not specified: '.$matches[1];
	                return '';
	            }
	            return rawurlencode($requestParams[$matches[1]]);
	        }, $path);

	        if (count($errors)) $this->errors = array_merge( $this->errors, $errors );
	        return $path;
	    }

		/** The main interface function
		 * @param $method(string): REST API GET, PUT, POST etc.
		 * @param $path(string): As per Google REST IP Spec
		 * @param $queryParams: Associative array. URL encoded and appended to URL
		 * @param $pathParams: Associative array. Used to replace {arg1},{arg2} in $path string
		 * @param $body: Associative array. Again, see Google REST API spec.
		 */
	    public function doRequest($method, $path, $queryParams=array(), $pathParams=array(), $body='') {

	        if (is_array($body)) {
	            // Treat an empty array in the body data as if no body data was set
	            if (!count($body)) $body = '';
	            else $body = json_encode( $body );
	        }

	        $this->errors = array();
	        $this->responseCode = 0;

	        $path = $this->pathReplace( $path, $pathParams );

	        if (count($this->errors)) return false;

	        $method = strtoupper($method);
	        $url = self::SERVICE_ENDPOINT.$path;

	        // Add on any query parameters
	        if (count($queryParams)) $url .= '?'.http_build_query($queryParams);

	        $ch = curl_init();

	        curl_setopt($ch,CURLOPT_URL,$url);
	        curl_setopt($ch,CURLOPT_HTTPHEADER,$this->headers());
	        curl_setopt($ch,CURLOPT_TIMEOUT,$this->timeout);
	        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	        if (in_array($method,array('DELETE','PATCH','POST','PUT'))) {

	            // All except DELETE can have a payload in the body
	            if ($method!='DELETE' && strlen($body)) {
	                curl_setopt($ch, CURLOPT_POST, true );
	                curl_setopt($ch, CURLOPT_POSTFIELDS, $body );
	            }

	            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
	        }

			if($this->sheetsApiDebug) {
				// CURLOPT_VERBOSE: TRUE to output verbose information. Writes output to STDERR,
				// or the file specified using CURLOPT_STDERR.
				curl_setopt($ch, CURLOPT_VERBOSE, true);
				$verbose = fopen(dirname( __DIR__ ).'/SheetsApiWrapper/sheetsApi.log', 'w+');
				curl_setopt($ch, CURLOPT_STDERR, $verbose);
				curl_setopt($ch, CURLINFO_HEADER_OUT, true);

			}

	        $result = curl_exec($ch);
			if($this->sheetsApiDebug) {
				Logging::Instance()->log('SheetsApi',"doRequest() ".var_export(curl_getinfo($ch),true));
			}

	        $contentType = curl_getinfo($ch,CURLINFO_CONTENT_TYPE);
	        $this->responseCode = curl_getinfo($ch,CURLINFO_HTTP_CODE);
			curl_close($ch);
			fclose($verbose);
	        return json_decode($result,true);
	    }

	    // Returns the errors responseCode returned from the last call to doRequest
	    function requestErrors() {
	        return $this->errors;
	    }

	}
}
/* END OF FILE */
?>
