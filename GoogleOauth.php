<?php
/*
 * @author Philip Wooldridge, based on the William Tam's version
 * @since 1.0.0
 *
 * Based on this blog: https://ieg.wnet.org/2015/08/using-oauth-in-wordpress-plugin-part-1-the-basics/ and
 * https://ieg.wnet.org/2015/09/using-oauth-in-wordpress-plugins-part-2-persistenc
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
if ( ! defined( 'ABSPATH' ) ) exit;
class OauthClient {
	private $dir;
    private $file;
    private $token;

	/**
	 * This code makes the class a singleton, only instantiated once when
	 * ::Instance() is invoked. Calling Instance again will
	 * return the same instance pointer.
	 */
	public static function Instance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new OauthClient( __FILE__ );
		}
		return $inst;
	}


    public function __construct( $file ) {
      $this->dir = dirname( $file );
      $this->file = $file;
      $this->token = 'google_oauth_sheets';

      // Register plugin settings
      add_action( 'admin_init' , array( $this , 'register_settings' ) );
      // Add settings page to menu
      add_action( 'admin_menu' , array( $this , 'add_menu_item' ) );
      // Add settings link to plugins page
      add_filter( 'plugin_action_links_' . plugin_basename( $this->file ) , array( $this , 'add_settings_link' ) );

      // setup meta boxes
      add_action( 'save_post', array( $this, 'meta_box_save' ) );

      // NEW: setup the wp ajax action for oAuth code exchange
      add_action( 'wp_ajax_google_finish_code_exchange', array($this, 'finish_code_exchange') );
      // NEW: setup the wp ajax action to logout from oAuth
      add_action( 'wp_ajax_google_logout_from_google', array($this, 'logout_from_google') );

	  register_deactivation_hook(__FILE__, array($this, 'google_deactivation'));

    }

    /* The next few functions set up the settings page */

    public function add_menu_item() {
      add_options_page( 'Google Oauth API Settings' , 'Google Oauth API Settings' , 'manage_options' , 'google_oauth_settings' ,  array( $this , 'settings_page' ) );
    }

    public function add_settings_link( $links ) {
      $settings_link = '<a href="options-general.php?page=google_oauth_settings">Settings</a>';
      array_push( $links, $settings_link );
      return $links;
    }

    public function register_settings() {
      register_setting( 'google_oauth_sheets_group', 'google_oauth_settings' );
      add_settings_section('settingssection1', 'Google App Settings', array( $this, 'settings_section_callback'), 'google_oauth_settings');
      // you can define EVERYTHING to create, display, and process each settings field as one line per setting below.  And all settings defined in this function are stored as a single serialized object.
      add_settings_field( 'google_app_client_id', 'Google App Client ID', array( $this, 'settings_field'), 'google_oauth_settings', 'settingssection1', array('setting' => 'google_oauth_settings', 'field' => 'google_app_client_id', 'label' => '', 'class' => 'regular-text') );
      add_settings_field( 'google_app_client_secret', 'Google App Client Secret', array( $this, 'settings_field'), 'google_oauth_settings', 'settingssection1', array('setting' => 'google_oauth_settings', 'field' => 'google_app_client_secret', 'label' => '', 'class' => 'regular-text') );
      add_settings_field( 'google_app_redirect_uri', 'Google App Redirect URI', array( $this, 'settings_field'), 'google_oauth_settings', 'settingssection1', array('setting' => 'google_oauth_settings', 'field' => 'google_app_redirect_uri', 'label' => '', 'class' => 'regular-text') );
    }

    public function settings_section_callback() { echo ' '; }

    public function settings_field( $args ) {
      // This is the default processor that will handle standard text input fields.  Because it accepts a class, it can be styled or even have jQuery things (like a calendar picker) integrated in it.  Pass in a 'default' argument only if you want a non-empty default value.
      $settingname = esc_attr( $args['setting'] );
      $setting = get_option($settingname);
      $field = esc_attr( $args['field'] );
      $label = esc_attr( $args['label'] );
      $class = esc_attr( $args['class'] );
      $default = ((isset($args['default']) && $args['default']) ? esc_attr( $args['default'] ) : '' );
      $value = (($setting[$field] && strlen(trim($setting[$field]))) ? $setting[$field] : $default);
      echo '<input type="text" name="' . $settingname . '[' . $field . ']" id="' . $settingname . '[' . $field . ']" class="' . $class . '" value="' . $value . '" /><p class="description">' . $label . '</p>';
    }

    public function settings_page() {
      if (!current_user_can('manage_options')) {
        wp_die( __('You do not have sufficient permissions to access this page.') );
      }
      ?>
      <div class="wrap">
        <h2>Google Sheets API Settings</h2>
        <p>You'll need to go to the <a href="https://console.developers.google.com">Google Developer Console</a> to setup your project and setup the values below.</p>
        <form action="options.php" method="POST">
          <?php settings_fields( 'google_oauth_sheets_group' ); ?>
          <?php do_settings_sections( 'google_oauth_settings' ); ?>
          <?php submit_button(); ?>
        </form>
      <!-- We handle the login process on the settings page now -->
      <?php $this->write_out_oAuth_JavaScript(); ?>
      </div>
      <?php

    }

    // This function is the clearest way to get the oAuth JavaScript onto a page as needed.
    private function write_out_oAuth_JavaScript() {
		$settings = get_option('google_oauth_settings', true);
		?>
		<script language=javascript>
		// we declare this variable at the top level scope to make it easier to pass around
		var google_access_token = "<?php echo $this->get_google_access_token(); ?>";

		jQuery(document).ready(function($) {
			var GOOGLECLIENTID = "<?php echo $settings['google_app_client_id']; ?>";
			var GOOGLECLIENTREDIRECT = "<?php echo $settings['google_app_redirect_uri']; ?>";
			// we don't need the client secret for this, and should not expose it to the web.

			function requestGoogleoAuthCode() {
				var OAUTHURL = 'https://accounts.google.com/o/oauth2/auth';

				var SCOPE = 'profile email openid https://www.googleapis.com/auth/spreadsheets';
				var popupurl = OAUTHURL + '?scope=' + SCOPE + '&client_id=' + GOOGLECLIENTID + '&redirect_uri=' + GOOGLECLIENTREDIRECT + '&response_type=code&access_type=offline&prompt=select_account consent';
				var win =   window.open(popupurl, "googleauthwindow", 'width=800, height=600');
				var pollTimer = window.setInterval(function() {
				try {
					  if (win.document.URL.indexOf(GOOGLECLIENTREDIRECT) != -1) {
					    window.clearInterval(pollTimer);
					    var response_url = win.document.URL;
					    var auth_code = gup(response_url, 'code');
					    //console.log(response_url);
					    win.close();
					    // We don't have an access token yet, have to go to the server for it
					    var data = {
					      action: 'google_finish_code_exchange',
					      auth_code: auth_code
					    };
					    $.post(ajaxurl, data, function(response) {
					      //console.log(response);
					      google_access_token = response;
					      getGoogleUserInfo(google_access_token);
					    });
					  }
					} catch(e) {}
				}, 500);
			}

			// helper function to parse out the query string params
			function gup(url, name) {
				name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
				var regexS = "[\\?#&]"+name+"=([^&#]*)";
				var regex = new RegExp( regexS );
				var results = regex.exec( url );
				if( results == null )
					return "";
				else
					return results[1];
			}

			function getGoogleUserInfo(google_access_token) {
				$.ajax({
					url: 'https://www.googleapis.com/plus/v1/people/me/openIdConnect',
					data: {
					  access_token: google_access_token
					},
					success: function(resp) {
					  $('#googleUserName').text('You are logged in. ');
					  loggedInToGoogle = true;
					  $('#google-login-block').hide();
					  $('#google-logout-block').show();
					},
					dataType: "jsonp"
				});
			}

			function logoutFromGoogle() {
				$.ajax({
				url: ajaxurl,
				data: {
				  action: 'google_logout_from_google'
				},
				success: function(resp) {
					  // console.log(resp);
					  $('#googleUserName').text(resp);
					  $('#google-login-block').show();
					  $('#google-logout-block').hide();
					  google_access_token = '';
					}
				});
			}

			// We also want to setup the initial click event and page status on document.ready
			$(function() {
				$('#google-login-block').click(requestGoogleoAuthCode);
				$('#google-logout-block').hide();
				$('#google-logout-block').click(logoutFromGoogle);
				// now lets show that they're logged in if they are
				if (google_access_token) {
					getGoogleUserInfo(google_access_token);
				}
			});
		});
		</script>
		<?php $LOGIN_BTN = plugin_dir_url(__FILE__).'/resources/btn_google_signin_dark_normal_web.png'; ?>

		<a id="google-login-block"><img alt="Login to Google" src="<?php echo $LOGIN_BTN; ?>"/></a>
		<span id="google-logout-block"><a>Logout from Google</a></span>
		<iframe id="googleAuthIFrame" style="visibility:hidden;" width=1 height=1></iframe>
		<?php
// END inlined JavaScript and HTML
    }
    /* NEW section to handle doing oAuth server-to-server */

    // wrapper for wp_ajax to point to reusable function
    public function finish_code_exchange() {
      $auth_code = ( isset( $_POST['auth_code'] ) ) ? $_POST['auth_code'] : '';
      echo $this->set_google_oauth2_token($auth_code, 'auth_code');
      wp_die();
    }

    private function set_google_oauth2_token($grantCode, $grantType) {
     /* based on code written by Jennifer L Kang that I found here
     * http://www.jensbits.com/2012/01/09/google-api-offline-access-using-oauth-2-0-refresh-token/
     * and modified to integrate with WordPress and to calculate and store the expiration date.
      */
      $settings = get_option('google_oauth_settings', true);
      $success = false;
      $oauth2token_url = "https://accounts.google.com/o/oauth2/token";
      $clienttoken_post = array(
        "client_id" => $settings['google_app_client_id'],
        "client_secret" => $settings['google_app_client_secret']
      );

      if ($grantType === "auth_code"){
        $clienttoken_post["code"] = $grantCode;
        $clienttoken_post["redirect_uri"] = $settings['google_app_redirect_uri'];
        $clienttoken_post["grant_type"] = "authorization_code";
      }
      if ($grantType === "refresh_token"){
        $clienttoken_post["refresh_token"] = get_option('google_refresh_token', true);
        $clienttoken_post["grant_type"] = "refresh_token";
      }
      $postargs = array(
        'body' => $clienttoken_post
       );
	  Logging::Instance()->log("GoogleOauth", "set_google_oauth2_token request: ".var_export($postargs,true)."");

      $response = wp_remote_post($oauth2token_url, $postargs );
      $authObj = json_decode(wp_remote_retrieve_body( $response ), true);
      if (isset($authObj['refresh_token'])){
        $refreshToken = $authObj['refresh_token'];
        $success = update_option('google_refresh_token', $refreshToken, false);
		// the final 'false' is so we don't autoload this value into memory on every page load
    	Logging::Instance()->log("GoogleOauth", "Refresh token updated: ".intval($success).": $refreshToken");
      } elseif (isset($authObj['access_token'])){
		$accessToken = $authObj['access_token'];
		$success = update_option('google_access_token', $accessToken, false);
		Logging::Instance()->log("GoogleOauth", "Access token updated: ".intval($success).": $accessToken");
	}
      if ($success) {
        $success = update_option('google_access_token_expires',  strtotime("+" . $authObj['expires_in'] . " seconds"));
      } else {
		  Logging::Instance()->log("GoogleOauth", "set_google_oauth2_token failed: ".var_export($authObj,true));
	  }
      if ($success) {
          $success = $authObj['access_token'];
      }
      // if there were any errors $success will be false, otherwise it'll be the access token
      if (!$success) { $success=false; }
      return $success;
    }

    public function get_google_access_token() {
      $expiration_time = get_option('google_access_token_expires', true);
      if (! $expiration_time) {
        return false;
      }
	  Logging::Instance()->log("GoogleOauth", "Access token expires on: ".date(DATE_COOKIE,$expiration_time)."");
      // Give the access token a 5 minute buffer (300 seconds)
      $expiration_time = $expiration_time - 300;
      if (time() < $expiration_time) {
		Logging::Instance()->log("GoogleOauth", "Returning existing access_token ");
        return get_option('google_access_token', true);
      }
      // at this point we have an expiration time but it is in the past or will be very soon
	  $new_token = $this->set_google_oauth2_token(null, 'refresh_token');
	  Logging::Instance()->log("GoogleOauth", "Returning new token: $new_token");

      return $new_token;
    }

    public function revoke_google_tokens() {
      /* This function finds either the access token or refresh token
       * revokes them with google (revoking the access token does the refresh too)
       * then deletes the data from the options table
      */
      $return = '';
      $token = get_option('google_access_token', true);
      $expiration_time = get_option('google_access_token_expires', true);
      if (!$token || (time() > $expiration_time)){
        $token = get_option('google_refresh_token', true);
      }
      if ($token) {
        $return = wp_remote_retrieve_response_code(wp_remote_get("https://accounts.google.com/o/oauth2/revoke?token=" . $token));
      } else {
        $return = "no tokens found";
      }
      if ($return == 200) {
        delete_option('google_access_token');
        delete_option('google_refresh_token');
        delete_option('google_access_token_expires');
        return true;
      } else {
        return $return;
      }
    }

    // wrapper for wp_ajax to point to reusable function
    public function logout_from_google() {
      $response = $this->revoke_google_tokens();
      if ($response === true) {
        $response = "success";
      }
      echo $response;
      wp_die();
    }

	function google_deactivation() {
      // delete the google tokens
      $plugin_obj = OauthClient::Instance();
      $plugin_obj->revoke_google_tokens();
      error_log('OauthClient has been deactivated');
    }

	public function get_access_token() {
		return $this->get_google_access_token();
	}
	//end of class
  }

/* END OF FILE */
?>
