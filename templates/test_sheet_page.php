<?php
echo "<h1>Expects a sheet with a tab named 'A sheet with a long name, and sheet ID 1j5fbxpXbtFTOpwgvgB5DJa-JjKp2xHQLUqU0lKcb8UY'</h1>";

$api = SheetsApiWrapper::Instance();
$sheet = $api->getSheet('1j5fbxpXbtFTOpwgvgB5DJa-JjKp2xHQLUqU0lKcb8UY');
echo "<p>Sheet = ".var_export($sheet,true)."</p>";

$values = [
    ["Test", "field", "$2", date(DATE_COOKIE)],
  ];
echo "<p>Append...</p>";

$result = $api->appendRows('1j5fbxpXbtFTOpwgvgB5DJa-JjKp2xHQLUqU0lKcb8UY',
			"'A sheet with a long name'!A1:D1",
			$values);
echo "<p>Append result = ".var_export($result,true)."</p>";
echo "<p>Errors=".var_export(GoogleSheetsApi::Instance()->requestErrors(),true)."</p>";

$result = $api->updateValues('1j5fbxpXbtFTOpwgvgB5DJa-JjKp2xHQLUqU0lKcb8UY',
"'A sheet with a long name'!A1",
			[[date(DATE_COOKIE)]]);
echo "<p>Update result = ".var_export($result,true)."</p>";
echo "<p>Errors=".var_export(GoogleSheetsApi::Instance()->requestErrors(),true)."</p>";

$result = $api->getValues('1j5fbxpXbtFTOpwgvgB5DJa-JjKp2xHQLUqU0lKcb8UY',
"'A sheet with a long name'!A1:D1");
echo "<p>Get result = ".var_export($result,true)."</p>";
echo "<p>Errors=".var_export(GoogleSheetsApi::Instance()->requestErrors(),true)."</p>";
